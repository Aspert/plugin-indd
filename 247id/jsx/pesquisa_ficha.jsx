

if(app.documents.length > 0){
	try{
		app.activeDocument.xmlElements.item(0).xmlElements.item("ficha").markupTag.name;
		var existeElementoFicha = true;
	}
	catch(e){
		var existeElementoFicha = false;
	}

	if(existeElementoFicha == true){
		var arrOrdem = new Array();

		var objDialogo = new Window("dialog", "Pesquisa");
		with(objDialogo){
			orientation = "row";

			add("statictext", undefined, "Pagina: ");

			var dropdownPagina = add('dropdownlist');
			dropdownPagina.removeAll();
			dropdownPagina.preferredSize = [75,25];
			dropdownPagina.selection = 0;

			for(var p = 0; p <= app.activeDocument.pages.length - 1; p++){
				dropdownPagina.add("item", (p+1));
			}

			add("statictext", undefined, "   Ordem:");

			var dropdownOrdem = add('dropdownlist');
			dropdownOrdem.preferredSize = [75,25];
			dropdownOrdem.selection = 0;

			dropdownPagina.onChange = function(){
				arrOrdem.length = 0;
				dropdownOrdem.removeAll();

				if(dropdownPagina.selection != null){
					for(i = 0; i <= app.activeDocument.xmlElements.item(0).xmlElements.length - 1; i++){
						if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).markupTag.name == "ficha"){
							if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlAttributes.item("extra").value.indexOf(dropdownPagina.selection.index+1 + "-") > -1){
								for(y = 0; y <= app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.length - 1; y++){
									if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(y).xmlContent.constructor.name != "Text"){
										var extra = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlAttributes.item("extra").value.split("-");
										if(extra.length == 2){
											arrOrdem.push(extra[1]);
										}
										break;
									}
								}
							}
						}
					}

					arrOrdem.sort(function (a, b){return (a - b);});
					for(i = 0; i <= arrOrdem.length - 1; i++){
						dropdownOrdem.add("item", parseInt(arrOrdem[i]));
					}
				}
			}

			objDialogo.okButton = add("button", undefined, "OK", {name:"ok"});
		}

		app.scriptPreferences.userInteractionLevel = UserInteractionLevels.INTERACT_WITH_ALL;

		if(objDialogo.show() == true){
			if((dropdownPagina.selection != null) && (dropdownOrdem.selection != null)){
				var panel = createProgress (360);
				panel.show();
				panel.pbar.value = 0;
				panel.pbar.maxvalue = (app.activeDocument.xmlElements.item(0).xmlElements.length - 1);
				panel.label.text = "Pesquisando...";

				var encontrado = false;
				var extraPesquisado = dropdownPagina.selection.index+1 + "-" + arrOrdem[dropdownOrdem.selection.index].toString();

				for(var i = 0;  i <= app.activeDocument.xmlElements.item(0).xmlElements.length - 1; i++){
					if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).markupTag.name == "ficha"){
						if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlAttributes.item("extra").value == extraPesquisado){
							var contadorSelecionados = 0;
							for(var y = 0; y <= app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.length - 1; y++){
								var objSelecionado = null;
								var construtor = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(y).xmlContent.constructor.name;

								if(construtor == "Story"){
									objSelecionado = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(y).xmlContent.texts.item(0).parentTextFrames[0];
								}
								else if((construtor == "Image") || (construtor == "EPS")){
									objSelecionado = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(y).xmlContent.parent;
								}
								else{
									objSelecionado = app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlElements.item(y).xmlContent;
								}

								if(objSelecionado != null){
									if((objSelecionado.constructor.name == "Rectangle") || (objSelecionado.constructor.name == "Polygon") || (objSelecionado.constructor.name == "TextFrame")){
										if(contadorSelecionados > 0){
											objSelecionado.select(SelectionOptions.ADD_TO);
										}
										else{
											objSelecionado.select();
											contadorSelecionados++;
										}
									}
								}
							}
							app.activeDocument.layoutWindows.item(0).zoomPercentage = app.activeDocument.layoutWindows.item(0).zoomPercentage;
							encontrado = true;
							break;
						}
					}
					panel.pbar.value++;
				}
				if(encontrado == false){
					alert("nehuma ficha encontrada!");
				}

				panel.hide();
				panel.close();
			}
			else{
				alert("Pagina ou ordem invalidas para pesquisa!");
			}
		}		
	}
	else{
		alert("Nenhuma ficha encontrada!");
	}
}
else{
	alert("Nenhum documento aberto!");
}
