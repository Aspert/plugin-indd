var scripts = Array();
scripts.push("main.jsx");
scripts.push("update_java.jsx");
scripts.push("update_layout.jsx");
scripts.push("valida_template.jsx");
scripts.push("salva_como.jsx");
scripts.push("verifica_conexao.jsx");
scripts.push("pesquisa_ficha.jsx");
scripts.push("informacoes_ficha.jsx");
scripts.push("itens_cenario.jsx");
scripts.push("update_cenarios.jsx");
scripts.push("gerar_indd.jsx");

var panel = createProgress (360);
panel.show();
panel.pbar.value = 0;
panel.pbar.maxvalue = scripts.length;
panel.label.text = "Atualizando Plugin";

for ( var i = 0; i <= scripts.length - 1; i++ ){
	panel.pbar.value++;
	atualizaScript(scripts[i], pluginPath, null);
	sleep(3000);
}

panel.pbar.value++;
sleep(3000);

var folder = new Folder(pluginPath);

var tiposArquivos = Array();
tiposArquivos.push("bat");
tiposArquivos.push("sh");

for ( var i = 0; i <= tiposArquivos.length - 1; i++ ){
	for ( var y = folder.getFiles("*." + tiposArquivos[i]).length - 1; y >= 0; y-- ){
		folder.getFiles("*." + tiposArquivos[i])[y].remove();
	}
}

panel.hide();
panel.close();