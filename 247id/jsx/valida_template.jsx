if(app.documents.length > 0){
	var arrLayer = new Array();
	var arrElementoRepetido = new Array();
	var erro = "";

	arrLayer[0] = "ficha";
	arrLayer[1] = "master";

	for(i = 0; i <= arrLayer.length - 1; i++){
		try{
			app.activeDocument.layers.item(arrLayer[i]);
			if(app.activeDocument.layers.item(arrLayer[i]).groups.length <= 0){
				erro += "Nenhum grupo encontrado para o layer '" +arrLayer[i]+ "'!;\n" ;
			}
			else if(arrLayer[i] == "master" && app.activeDocument.layers.item(arrLayer[i]).groups.length > 1){
				erro += "Mais de um grupo encontrado para o layer 'master'!;\n" ;
			}
		}
		catch(e){
			erro += "Layer '" +arrLayer[i]+ "' nao existe!;\n" ;
		}
	}

	try{
		app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").untag();
		app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").remove();
	}
	catch(e){}

	try{
		app.activeDocument.xmlTags.item("jobinfo").remove(app.activeDocument.xmlTags.anyItem());
	}
	catch(e){}

	app.scriptPreferences.userInteractionLevel = UserInteractionLevels.INTERACT_WITH_ALL;

	var panel = createProgress (360);
	panel.show();
	panel.pbar.value = 0;
	panel.pbar.maxvalue = app.activeDocument.groups.length;
	panel.label.text = "Verificando elementos duplicados..." ;
	var contadorFichaSubgrupo = 0;
	var contadorMasterSubgrupo = 0;

	for(g = 0; g <= app.activeDocument.groups.length - 1; g++){
		panel.label.text = "Verificando elementos duplicados... (Grupo " + (g+1) + " de " + (app.activeDocument.groups.length-1) + ")";
		if(app.activeDocument.groups.item(g).itemLayer.name == "master"){
			if (app.activeDocument.groups.item(g).parent.constructor.name != "Page"){
				app.activeDocument.groups.item(g).move( [app.activeDocument.pages.item(0).bounds[0], app.activeDocument.pages.item(0).bounds[1]] );
				alert("A tag master foi movida para dentro da pagina");
			}
			if(app.activeDocument.groups.item(g).groups.length > 0){
				app.activeDocument.groups.item(g).select(SelectionOptions.ADD_TO);
				contadorMasterSubgrupo++;
				erro += "Grupo 'master' contem 'subgrupo(s)'!;\n";
			}
			else{
				arrElementoRepetido = elementoXmlRepetidoNoGrupo(app.activeDocument.groups.item(g));
				if(arrElementoRepetido.length > 0){
					for(i = 0; i <= arrElementoRepetido.length - 1; i++){
						erro += "Elemento '" +arrElementoRepetido[i]+ "' repetido no layer 'master'!;\n";
					}
				}
			}
		}
		else if(app.activeDocument.groups.item(g).itemLayer.name == "ficha"){
			if(app.activeDocument.groups.item(g).groups.length > 0){
				app.activeDocument.groups.item(g).select(SelectionOptions.ADD_TO);
				contadorFichaSubgrupo++
			}
			else{
				arrElementoRepetido = elementoXmlRepetidoNoGrupo(app.activeDocument.groups.item(g));
				if(arrElementoRepetido.length > 0){
					for(i = 0; i <= arrElementoRepetido.length - 1; i++){
						erro += "Elemento '" +arrElementoRepetido[i]+ "' repetido no layer 'ficha'!;\n";
					}
				}
			}
		}
		panel.pbar.value++;
	}

	if(contadorFichaSubgrupo > 0){
		erro += contadorFichaSubgrupo + " ficha(s) contem subgrupo(s)!;\n";
	}

	if(contadorMasterSubgrupo > 0){
		erro += "Master contem subgrupo(s)!;\n";
	}

	panel.pbar.value = 0;
	panel.pbar.maxvalue = (app.activeDocument.xmlElements.item(0).xmlElements.length - 1);
	panel.label.text = "Verificando elementos vazios...";

	var deletaFichaInutil = null;
	var contador = 1;
	var totalFichas = app.activeDocument.xmlElements.item(0).xmlElements.length;
	for(var i = app.activeDocument.xmlElements.item(0).xmlElements.length - 1; i >= 0; i--){
		panel.label.text = "Verificando elementos vazios... (Elemento " + contador + " de " + totalFichas + ")";
		if(app.activeDocument.xmlElements.item(0).xmlElements.item(i).xmlContent.constructor.name == "Text"){
			if(deletaFichaInutil == null){
				if(confirm("Foram encontrados elementos sem associacao, isto pode causar lentidao na geracao do template. Deseja exclui-los?")){
					deletaFichaInutil = true;
					app.activeDocument.xmlElements.item(0).xmlElements.item(i).remove();
				}
				else{
					deletaFichaInutil = false;
					break;
				}
			} else if(deletaFichaInutil == true){
				app.activeDocument.xmlElements.item(0).xmlElements.item(i).remove();
			}
		}
		panel.pbar.value++;
		contador++;
	}

	panel.hide();
	panel.close();

	if(erro != ""){
		alert("Validacao de template\n" +erro);
	}
	else{
		alert("Nenhum erro encontrado no template!");
	}
}
else{
	alert("Nenhum documento aberto");
}