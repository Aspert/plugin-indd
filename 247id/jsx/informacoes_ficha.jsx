if(app.documents.length > 0){
	if(app.activeDocument.selection.length == 1){
		var encontrado = false;
		var objSelecionado = null;
		var construtor = app.activeDocument.selection[0].constructor.name;

		if(construtor == "Story"){
			objSelecionado = app.activeDocument.selection[0].texts.item(0).parentTextFrames[0];
		}
		else if((construtor == "Image") || (construtor == "EPS")){
			objSelecionado = app.activeDocument.selection[0].parent;
		}
		else if(construtor == "Group"){
			var objetoValido = false;
			var tiposObjetos = new Array();
			tiposObjetos.push(app.activeDocument.selection[0].rectangles);
			tiposObjetos.push(app.activeDocument.selection[0].textFrames);
			tiposObjetos.push(app.activeDocument.selection[0].polygons);

			for(var i = 0; i <= tiposObjetos.length - 1; i++){
				for(var y = 0; y <= tiposObjetos[i].length - 1; y++){
					if(tiposObjetos[i].item(y).associatedXMLElement){
						if(tiposObjetos[i].item(y).associatedXMLElement.parent.constructor.name == "XMLElement"){
							if(tiposObjetos[i].item(y).associatedXMLElement.parent.markupTag.name == "ficha"){
								objSelecionado = tiposObjetos[i].item(y);
								objetoValido = true;
								break;
							}
						}
					}
				}
				if(objetoValido == true){
					break;
				}
			}
		}
		else{
			objSelecionado = app.activeDocument.selection[0];
		}

		if(objSelecionado != null){
			if((objSelecionado.constructor.name == "Rectangle") || (objSelecionado.constructor.name == "Polygon") || (objSelecionado.constructor.name == "TextFrame")){
				if(objSelecionado.associatedXMLElement){
					if(objSelecionado.associatedXMLElement.parent.constructor.name == "XMLElement"){
						if(objSelecionado.associatedXMLElement.parent.markupTag.name == "ficha"){
							var xmlFicha = objSelecionado.associatedXMLElement.parent;
							var arrElementoNome = new Array();
							var arrElementoValor = new Array();
							encontrado = true;

							for(var i = 0; i <= xmlFicha.xmlElements.length - 1; i++){
								var elementoNome = "";
								var elementoValor = "";
								var construtor = xmlFicha.xmlElements.item(i).xmlContent.constructor.name;

								if(construtor != "Text"){
									if(construtor == "Story"){
										try{
											elementoNome = xmlFicha.xmlElements.item(i).markupTag.name;
											elementoValor = xmlFicha.xmlElements.item(i).xmlAttributes.item("conteudo").value;
											arrElementoNome.push(elementoNome);
											arrElementoValor.push(elementoValor);
										}catch(e){}
									}
									else{
										try{
											elementoNome = xmlFicha.xmlElements.item(i).markupTag.name;
											elementoValor = xmlFicha.xmlElements.item(i).xmlAttributes.item("href").value;
											elementoValor = decodeURI(File (elementoValor).name);
											arrElementoNome.push(elementoNome);
											arrElementoValor.push(elementoValor);
										}catch(e){}
									}
								}
							}

							var objDialogo = new Window("dialog", "Informacoes");
							with(objDialogo){
								alignChildren = "fill";
								var painel = add("panel", undefined, "Ficha:");
								with(painel){
									orientation = "column";
									alignChildren = "left";
									margins = [12, 14, 12, 6];
									spacing = 4;
									with(add("group")){
										orientation = "row";
										alignChildren = "center";
										spacing = 30;
										with(add("group")){
											orientation = "column";
											alignChildren = "left";

											add("statictext", undefined, "ID:");	
											add("statictext", undefined, "Pagina:");
											add("statictext", undefined, "Ordem");
										}
										with(add("group")){
											orientation = "column";
											alignChildren = "right";
											
											try{
												var id = xmlFicha.xmlAttributes.item("id").value.toString();
											}
											catch(e){
												var id = "";
											}
											objDialogo.ficha = add("edittext", undefined, id, null);
											objDialogo.ficha.characters = 17;
											objDialogo.ficha.justify = "right";
											objDialogo.ficha.enabled = false;

											try{
												var pagina = xmlFicha.xmlAttributes.item("pagina").value.toString();
											}
											catch(e){
												var pagina = "";
											}
											objDialogo.ficha = add("edittext", undefined, pagina, null);
											objDialogo.ficha.characters = 17;
											objDialogo.ficha.justify = "right";
											objDialogo.ficha.enabled = false;

											try{
												var ordem = xmlFicha.xmlAttributes.item("extra").value.toString();
												var arr = ordem.split("-");
												if(arr.length == 2){
													ordem = arr[1];
												}
												else{
													ordem = "";
												}
											}
											catch(e){
												var ordem = "";
											}
											objDialogo.ficha = add("edittext", undefined, ordem, null);
											objDialogo.ficha.characters = 17;
											objDialogo.ficha.justify = "right";
											objDialogo.ficha.enabled = false;
										}
									}
								}

								var painel = add("panel", undefined, "Tags:");
								with(painel){
									if((arrElementoNome.length > 0) && (arrElementoValor.length > 0)){
										orientation = "column";
										alignChildren = "left";
										margins = [12, 14, 12, 6];
										spacing = 4;
										with(add("group")){
											orientation = "row";
											alignChildren = "center";
											spacing = 30;
											with(add("group")){
												orientation = "column";
												alignChildren = "left";

												for(var i = 0; i <= arrElementoNome.length - 1; i++){
													add("statictext", undefined, arrElementoNome[i]);
												}
											}
											with(add("group")){
												orientation = "column";
												alignChildren = "right";
												
												for(var i = 0; i <= arrElementoValor.length - 1; i++){
													objDialogo.ficha = add("edittext", undefined, arrElementoValor[i], null);
													objDialogo.ficha.characters = 17;
													objDialogo.ficha.justify = "right";
													objDialogo.ficha.enabled = false;
												}
											}
										}
									}
								}
							
								with(add("group")){
									orientation = "row";
									alignment = "right";
									objDialogo.okButton = add("button", undefined, "OK", {name:"ok"});
								}
							}
							
							app.scriptPreferences.userInteractionLevel = UserInteractionLevels.INTERACT_WITH_ALL;
							objDialogo.show();
						}
					}
				}
			}
		}
		if(encontrado == false){
			alert("Nenhuma ficha encontrada para esse elemento!");
		}
	}
	else if(app.activeDocument.selection.length > 1){
		alert("Existe mais de um item selecionado.\nPara obter informações de uma ficha, selecione apenas um item da mesma!");
	}
	else if(app.activeDocument.selection.length == 0){
		alert("Para obter informações de uma ficha, selecione um item da mesma!");
	}
}
else{
	alert("Nenhum documento aberto");
}