// CRIACAO DE BARRA DE PROGRESSO
function createProgress(width){
	var ppanel = new Window("window", "Atualizando arquivo...");
	ppanel.g = ppanel.add("group");
	ppanel.g.orientation = "column";
	ppanel.label = ppanel.g.add("statictext", [12, 12, width, 26]);
	ppanel.label.text="Parcial";
	ppanel.pbar = ppanel.g.add("progressbar", [12, 12, width, 24], 0, 1);
	return ppanel;
}

if(Folder(pluginPath + "jsx").exists){
	app.scriptPreferences.userInteractionLevel = UserInteractionLevels.INTERACT_WITH_ALL;

	var panel = createProgress (360);
	panel.show();
	panel.pbar.value = 0;
	panel.pbar.maxvalue = 10;
	panel.label.text = "Atualizando Plugin";

	panel.pbar.value++;
	atualizaScript("main.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	atualizaScript("update_java.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	atualizaScript("salva_como.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	atualizaScript("verifica_conexao.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	atualizaScript("pesquisa_ficha.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	atualizaScript("informacoes_ficha.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	atualizaScript("update_cenarios.jsx", pluginPath);
	sleep(3000);
	
	panel.pbar.value++;
	atualizaScript("gerar_indd.jsx", pluginPath);
	sleep(3000);
	
	panel.pbar.value++;
	incluiScript("main.jsx", pluginPath);
	sleep(3000);

	panel.pbar.value++;
	sleep(3000);
	
	var folder = new Folder(pluginPath);
	var files = folder.getFiles("*.sh");
	if(folder.getFiles("*.bat").length > 0){
		files.push(folder.getFiles("*.bat"));
	}
	
	for(var f = files.length - 1; f >= 0; f--){
		files[f].remove();
	}
	
	panel.hide();
	panel.close();
}