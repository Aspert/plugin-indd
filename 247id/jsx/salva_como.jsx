if(app.documents.length > 0){
	app.scriptPreferences.userInteractionLevel = UserInteractionLevels.INTERACT_WITH_ALL;

	folder = new Folder(pluginPath);
	folder.create();

	if(folder.exists){
		try{jobinfo = replaceAll(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").markupTag.name, " ", "_");}
		catch(e){jobinfo = ""}
		try{idJob = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_job").value;}
		catch(e){idJob = ""}
		try{idPraca = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_praca").value;}
		catch(e){idPraca = ""}
		try{idExcel = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("id_excel").value;}
		catch(e){idExcel = ""}
		try{servidor = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("servidor").value;}
		catch(e){servidor = ""}
		try{paginaInicio = parseInt(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("pagina_inicio").value);}
		catch(e){paginaInicio = 0}
		try{paginaFim = parseInt(app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo").xmlAttributes.item("pagina_fim").value);}
		catch(e){paginaFim = 0}

		if((jobinfo != "") && (idJob != "") && (idPraca != "") && (idExcel != "") && (servidor != "")){
			folder = new Folder(pluginPath + "" + idJob + "_" + idPraca);
			deletaDiretorio(folder) ;
			folder.create();

			//RETORNA O XML DE FICHAS
			if(File.fs == "Windows"){
				var batText = "@echo off\n";
				batText += "echo Atualizando Pracas\n";
				batText += "@java -jar " + pluginPath + barra + "plugin.jar \"atualiza_pracas\" \"" + pluginPath + "\" \"" + servidor + "\" \"" + idJob + "\" \"" + idPraca + "\" \"" + idExcel + "\"";
				var fileExec = new File(pluginPath + idJob + "_" + idPraca + barra + "pracas.bat");
				fileExec.open("w");
				fileExec.write(batText);
				fileExec.close();
				fileExec.execute();
			}
			else{
				var shText = "java -jar " + pluginPath + barra + "plugin.jar \"atualiza_pracas\" \"" + pluginPath + "\" \"" + servidor + "\" \"" + idJob + "\" \"" + idPraca + "\" \"" +idExcel + "\" \"" + paginaInicio.toString() +  "\" \"" + paginaFim.toString() + "\"; ";
				var shScript = pluginPath + idJob + "_" + idPraca + barra + "pracas.sh";
				var fileExec = new File(shScript);
				fileExec.open("w+x");
				fileExec.write(shText);
				fileExec.close();
				var appleScript = "do shell script \"sh '" + shScript + "' \"\n ";
				app.doScript(appleScript, ScriptLanguage.APPLESCRIPT_LANGUAGE);
			}

			//AGUARDA FINALIZACAO DAS TAREFAS DO JAVA. CASO O ARQUIVO pracas.pid SEJA CRIADO, FORAM FINALIZADA AS TAREFAS
			do{
				var pidFile = new File(pluginPath + "" + idJob + "_" + idPraca + barra + "pracas.pid");
				var errFile = new File(pluginPath + "" + idJob + "_" + idPraca + barra + "pracas.xml.err");
				var errControle = new File(pluginPath + barra + "controle.err");

				if(errFile.exists){
					alert("Nao foi possivel importar o XML de pra�as");
					break;
				}
				errFile.close();

				if(errControle.exists){
					alert("Nao foi possivel setar ambiente");
					break;
				}
				errControle.close();
			} while(!pidFile.exists) ;

			fileExec.remove();

			var objXmlRootExterno = importaXml(pluginPath + "" + idJob + "_" + idPraca + barra + "pracas.xml");
			if(objXmlRootExterno.elements().length() > 0){
				var arrPracas = new Array();
				arrPracas.push("");
				for(w = 0; w <= objXmlRootExterno.elements().length() - 1; w++){
					if(objXmlRootExterno.elements()[w].attribute("id").toString() != idPraca){
						arrPracas.push(objXmlRootExterno.elements()[w].attribute("id") + "-" + objXmlRootExterno.elements()[w].attribute("nome"));
					}
				}

				var objDialogo = app.dialogs.add({name:"Salvar Como", canCancel:true});
				with(objDialogo){
					with(dialogColumns.add()){
						staticTexts.add({staticLabel:"Selecione a Pra\u00E7a:"});
						var myDropdowns = dropdowns.add({stringList:arrPracas, selectedIndex:0});
					}
				}

				var resultado = objDialogo.show();
				if(resultado == true){
					if(myDropdowns.selectedIndex > 0){
						app.activeDocument.save();

						var objXmlJobinfo = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo");

						var arquivoOriginal = app.activeDocument.filePath + barra + app.activeDocument.name;

						var arr = arrPracas[myDropdowns.selectedIndex].split("-");
						var idPraca = arr[0].toString();
						var nomePraca = arr[1].toString();
						
						var objXmlJobinfo = app.activeDocument.xmlElements.item(0).xmlElements.item("jobinfo");

						objXmlJobinfo.xmlAttributes.item("id_praca").value = idPraca;

						app.activeDocument.save(new File(app.activeDocument.filePath + barra + idExcel + "-" + nomePraca + "-" + paginaInicio + "-" + paginaFim + ".indd"));
						
						alert("Salvo como '" +nomePraca+ "'");
					}
					else{
						alert("Selecione uma praca");
					}
				}
			}
			else{
				alert("Nehuma praca encontrada!");
			}
		}
		else{
			alert("Template invalido para Salvar Como outra praca!")
		}
	}
}
else{
	alert("Nenhum documento aberto");
}