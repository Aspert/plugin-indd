package br.com.id247.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom.input.SAXBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ArquivoHelper {
	public ArquivoHelper() {
	}

	public static ArrayList<String> getArrayArquivo(String file) {
		ArrayList<String> arrArquivo = new ArrayList();
		try {
			BufferedReader in = new BufferedReader(new java.io.FileReader(new java.io.File(file)));
			String str = "";

			while ((str = in.readLine()) != null) {
				arrArquivo.add(str);
			}
			in.close();
		} catch (FileNotFoundException e) {
			LogHelper.log(e.getMessage());
		} catch (java.io.IOException e) {
			LogHelper.log(e.getMessage());
		}
		return arrArquivo;
	}

	public static File removeElementos(File pid, String argumentos) {
		
		SAXBuilder builder = new SAXBuilder();
		try {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = (Document) db.parse(pid);
			
			Element rootNode = (Element) document.getDocumentElement();
			
			NodeList list = rootNode.getElementsByTagName("ficha");
			
			for (int i = 0; i < list.getLength(); i++) {
				Element node = (Element) list.item(i);
				NodeList atributos = node.getChildNodes();
				List<Node> removeNode = new ArrayList<Node>();
				
				
				for (int y = 0; y < atributos.getLength(); y++) {
					Node child =  atributos.item(y);
					 
					if(!argumentos.toLowerCase().contains(child.getNodeName().toLowerCase())){						
						removeNode.add(child);						
					}					
				}
				
				for(Node nodeRemove:removeNode){
					node.removeChild(nodeRemove);
				}
				
			}
			
			DOMSource source = new DOMSource(document);
		    FileWriter writer = new FileWriter(pid);
		    StreamResult result = new StreamResult(writer);

		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
		    Transformer transformer = transformerFactory.newTransformer();
		    transformer.transform(source, result);
			writer.close();

		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (Exception e){
			System.out.println(e.getMessage());
		}

		return pid;
	}

}