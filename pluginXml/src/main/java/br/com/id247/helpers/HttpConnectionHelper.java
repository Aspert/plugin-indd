package br.com.id247.helpers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HttpConnectionHelper {
	private static HttpURLConnection connection;

	public HttpConnectionHelper() {
	}

	public static HttpURLConnection getHttpURLConnection(String urlString) {
		try {
			URL hostUrl = new URL(urlString);

			connection = (HttpURLConnection) hostUrl.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-length", String.valueOf("valor=21"));
			connection.connect();

			return connection;
		} catch (MalformedURLException localMalformedURLException) {
		} catch (IOException localIOException) {
		} catch (NullPointerException localNullPointerException) {
		}

		return null;
	}

	public static boolean getEpsFile(HttpURLConnection connection, String params, String saveFile) {
		try {
			if (connection != null) {
				OutputStream os = connection.getOutputStream();
				OutputStreamWriter ow = new OutputStreamWriter(os);
				ow.write(params);
				ow.close();

				InputStream arquivo_remoto = connection.getInputStream();

				FileOutputStream arquivo_local = new FileOutputStream(saveFile);
				byte[] buf = new byte[1024];
				int len = 0;
				while ((len = arquivo_remoto.read(buf)) > 0) {
					arquivo_local.write(buf, 0, len);
				}

				arquivo_local.close();
				arquivo_remoto.close();

				return true;
			}
		} catch (IOException ex) {
			LogHelper.log(ex.getMessage());
		}

		return false;
	}

	public static boolean getLayout(HttpURLConnection connection, String params, String saveFile) {
		try {
			if (connection != null) {
				OutputStream os = connection.getOutputStream();
				OutputStreamWriter ow = new OutputStreamWriter(os);
				ow.write(params);
				ow.close();

				InputStream arquivo_remoto = connection.getInputStream();

				FileOutputStream arquivo_local = new FileOutputStream(saveFile);

				byte[] buf = new byte[1024];
				int len = 0;
				while ((len = arquivo_remoto.read(buf)) > 0) {
					arquivo_local.write(buf, 0, len);
				}

				arquivo_local.close();
				arquivo_remoto.close();

				return true;
			}
		} catch (IOException ex) {
			LogHelper.log(ex.getMessage());
		}

		return false;
	}

	public static boolean downloadArquivo(String url, String destino) {
		try {
			URL link = new URL(url);
			InputStream arquivo_remoto = link.openStream();
			FileOutputStream arquivo_local = new FileOutputStream(destino);

			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = arquivo_remoto.read(buf)) > 0) {
				arquivo_local.write(buf, 0, len);
			}

			arquivo_local.close();
			arquivo_remoto.close();

			return true;
		} catch (MalformedURLException localMalformedURLException) {
		} catch (IOException localIOException) {
		}

		return false;
	}

	public static boolean getContentOfFile(String _url, String fileDestino) {
		try {
			URL url = new URL(_url);
			InputStream arquivo_remoto = url.openStream();
			FileOutputStream arquivo_local = new FileOutputStream(fileDestino);

			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = arquivo_remoto.read(buf)) > 0) {
				arquivo_local.write(buf, 0, len);
			}

			arquivo_local.close();
			arquivo_remoto.close();

			return true;
		} catch (MalformedURLException localMalformedURLException) {
		} catch (IOException localIOException) {
		}

		return false;
	}

	public static String getContentOfFile(String _url) {
		StringBuilder retorno = new StringBuilder();
		try {
			URL url = new URL(_url);
			InputStream arquivo_remoto = url.openStream();
			BufferedInputStream reader = new BufferedInputStream(arquivo_remoto);
			byte[] buf = new byte[1024];
			int len = 0;

			while ((len = reader.read(buf, 0, buf.length)) != -1) {
				retorno.append(new String(buf));
			}

			arquivo_remoto.close();
		} catch (MalformedURLException localMalformedURLException) {
		} catch (IOException localIOException) {
		}

		return retorno.toString();
	}

	public static boolean verificaConexao(String url) throws IOException {
		try {
			URL mandarMail = new URL(url);
			URLConnection conn = mandarMail.openConnection();

			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.connect();
			int x = httpConn.getResponseCode();
			if (x == 200) {
				return true;
			}
		} catch (MalformedURLException urlmal) {
			return false;
		} catch (IOException ioexcp) {
			return false;
		}
		return false;
	}

	public static String getContentOfFileHttp(String url) throws IOException {
		String retorno = "";
		int redirs = 0;
		int maxRedirs = 3;
		HttpURLConnection conn = null;

		while (redirs <= maxRedirs) {
			URL hostUrl = new URL(url);
			conn = (HttpURLConnection) hostUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.connect();

			int code = conn.getResponseCode();
			if ((code >= 301) && (code <= 307)) {
				String location = conn.getHeaderField("location");
				url = location;
			} else {
				if (code == 200) {
					break;
				}

				redirs++;
			}
		}
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String linha = "";
			while ((linha = reader.readLine()) != null) {
				retorno = retorno + linha;
			}
			String location = conn.getHeaderField("location");
			int code = conn.getResponseCode();
			if ((code >= 301) && (code <= 307)) {
				conn.getHeaderField("location");
			}
		} catch (IOException e) {
			LogHelper.log(e.getMessage());
		}

		return retorno;
	}
}
