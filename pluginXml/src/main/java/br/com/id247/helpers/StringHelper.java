package br.com.id247.helpers;

import java.io.UnsupportedEncodingException;

public class StringHelper {
	public StringHelper() {
	}

	public static String removeExtensao(String arquivo) {
		int dotPos = arquivo.lastIndexOf(".");
		return arquivo.substring(0, dotPos);
	}

	public static String toUTF8(String s) throws UnsupportedEncodingException {
		byte[] utf8 = s.getBytes("UTF-8");
		return new String(utf8);
	}
}