package br.com.id247.helpers;

import br.com.id247.bo.ConfigBO;
import java.io.IOException;
import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class LogHelper {
	public LogHelper() {
	}

	public static void log(String msg) {
		log(msg, LogHelper.class);
	}

	public static void log(String msg, Object obj) {
		Logger logger = Logger.getLogger(obj.getClass());
		logger.setLevel(Level.INFO);

		BasicConfigurator.configure();
		try {
			Appender fileAppender = new FileAppender(new PatternLayout("%r [%t] %p %c %x - %m%n"),
					ConfigBO.ARQUIVO_LOG);
			logger.addAppender(fileAppender);
			logger.info(msg);
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}
}