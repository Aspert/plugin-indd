package br.com.id247.helpers;

import java.io.File;

public class DiretorioHelper {
	public DiretorioHelper() {
	}

	public static boolean deletaRecursivamente(File file) {
		boolean ok = false;

		if (file.isFile()) {
			file.delete();
		} else {
			file.delete();

			if (file.listFiles() != null) {
				File[] files = file.listFiles();
				for (int i = 0; i < files.length; i++) {
					deletaRecursivamente(files[i]);
				}
			}
		}

		return ok;
	}

	public static boolean deletaPidRecursivamente(File file) {
		boolean ok = false;

		if (file.isFile()) {
			file.delete();
		} else {
			file.delete();

			if (file.listFiles() != null) {
				File[] files = file.listFiles();
				for (int i = 0; i < files.length; i++) {
					if (files[i].getName().indexOf(".pid") >= 0) {
						deletaRecursivamente(files[i]);
					}
				}
			}
		}

		return ok;
	}
}