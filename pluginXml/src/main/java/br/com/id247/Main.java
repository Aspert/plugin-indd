package br.com.id247;

import br.com.id247.bo.AtualizaInicialXmlBO;
import br.com.id247.bo.AtualizaScriptJsxBO;
import br.com.id247.bo.AtualizaXmlLocalBO;
import br.com.id247.bo.AtualizaXmlPracasLocalBO;
import br.com.id247.bo.ConfigBO;
import br.com.id247.bo.DownloadFpoBO;
import br.com.id247.bo.DownloadLayoutBO;
import br.com.id247.bo.VerificaConexaoBO;
import br.com.id247.bo.VerificaLoginBO;
import br.com.id247.helpers.ArquivoHelper;
import br.com.id247.helpers.LogHelper;
import br.com.id247.helpers.StringHelper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Main {
	public Main() {
	}

	public static void main(String[] args) throws IOException {
		boolean create = false;

		if (args.length == 4) {
			create = true;
		}

		String acao = args[0];

		File fileErr = new File(args[1] + "/controle.err");

		String servidor = args[2];

		if (ConfigBO.setAmbiente(servidor, args[1], create)) {
			fileErr.delete();

			if (acao.equals("atualiza_fichas")) {
				String tipoAtualizacao = args[3];
				String idJob = args[4];
				String idPraca = args[5];
				String idExcel = args[6];
				String paginaInicio = args[7];
				String paginaFim = args[8];
				String argumentos = args[9];

				if (!AtualizaXmlLocalBO.atualiza(tipoAtualizacao, idJob, idPraca, idExcel, paginaInicio, paginaFim,
						argumentos)) {
					LogHelper.log("Nao foi possivel atualizar XML : ID_JOB: " + idJob + " - ID_PRACA: " + idPraca
							+ " - ID_EXCEL: " + idExcel);
				}
			} else if (acao.indexOf(".jsx") >= 0) {
				if (!AtualizaScriptJsxBO.atualiza(acao)) {
					LogHelper.log("Nao foi possivel atualizar script " + acao);
				}
			} else if (acao.equals("atualiza_pracas")) {
				String idJob = args[3];
				String idPraca = args[4];
				String idExcel = args[5];

				if (!AtualizaXmlPracasLocalBO.atualiza(idJob, idPraca, idExcel)) {
					LogHelper.log("Nao foi possivel atualizar XML : ID_JOB: " + idJob + " - ID_PRACA: " + idPraca
							+ " - ID_EXCEL: " + idExcel);
				}
			} else if (acao.equals("verifica_conexao")) {
				VerificaConexaoBO.verifica();
			} else if (acao.equals("data_template")) {
				String idJob = args[3];
				String idPraca = args[4];
				String idExcel = args[5];
				String idTemplate = args[6];
				String paginaInicio = args[7];
				String paginaFim = args[8];
				DownloadLayoutBO.getLayoutDate(idJob, idPraca, idTemplate, paginaInicio, paginaFim);
			} else if (acao.equals("tamanho_template")) {
				String idJob = args[3];
				String idPraca = args[4];
				String idExcel = args[5];
				String idTemplate = args[6];
				String paginaInicio = args[7];
				String paginaFim = args[8];
				DownloadLayoutBO.getLayoutSize(idJob, idPraca, idTemplate, paginaInicio, paginaFim);
			} else if (acao.equals("path_template")) {
				String idJob = args[3];
				String idPraca = args[4];
				String idExcel = args[5];
				String idTemplate = args[6];
				String paginaInicio = args[7];
				String paginaFim = args[8];
				DownloadLayoutBO.getLayoutPath(idJob, idPraca, idTemplate, paginaInicio, paginaFim);
			} else if (acao.equals("baixa_template")) {
				String idJob = args[3];
				String idPraca = args[4];
				String idExcel = args[5];
				String idTemplate = args[6];
				String paginaInicio = args[7];
				String paginaFim = args[8];

				JFrame frame = new JFrame();
				JTextArea area = new JTextArea(10, 40);
				area.setEditable(false);

				JScrollPane scroll = new JScrollPane(area, 22, 32);

				frame.setDefaultCloseOperation(0);
				frame.add(scroll);
				frame.pack();
				frame.setVisible(true);

				String tamanhoTotal = DownloadLayoutBO
						.getLayoutSize1(idJob, idPraca, idTemplate, paginaInicio, paginaFim).trim();

				area.append("Baixando Template...\n");
				area.append("Tamanho Total: " + tamanhoTotal + "MB \n");

				DownloadLayoutBO.getLayout(idJob, idPraca, idTemplate, paginaInicio, paginaFim);
			} else if (acao.equals("verifica_login")) {
				String usuario = args[3];
				String senha = args[4];
				String coordenadaCartao = args[5];
				String valorCartao = args[6];

				VerificaLoginBO.verificaLogin(usuario, senha, coordenadaCartao, valorCartao);
			} else if (acao.equals("inicial")) {
				String idUsuario = args[3];
				AtualizaInicialXmlBO.atualiza(idUsuario);
			} else if (acao.equals("baixa_fpo")) {
				File arquivo = new File(args[3]);
				String idJob = args[4];
				String idPraca = args[5];
				String tipo = args[7];
				File fileErrTotal = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/erro_total.err");
				fileErrTotal.delete();
				boolean controle = false;

				if (arquivo.isFile()) {
					JFrame frame = new JFrame();
					JTextArea area = new JTextArea(10, 40);
					area.setEditable(false);

					JScrollPane scroll = new JScrollPane(area, 22, 32);

					frame.setDefaultCloseOperation(0);
					frame.add(scroll);
					frame.pack();
					frame.setVisible(true);

					ArrayList<String> arquivos = ArquivoHelper.getArrayArquivo(arquivo.getAbsolutePath());

					Iterator<String> arquivoIterator = arquivos.iterator();

					while (arquivoIterator.hasNext()) {
						controle = true;
						String arquivoXinet = new String((String) arquivoIterator.next());
						String[] arquivosXinet = arquivoXinet.split("/");

						String filePath = "";
						for (int i = 0; i < arquivosXinet.length - 1; i++) {
							filePath = filePath + arquivosXinet[i] + "/";
						}

						String fileName = StringHelper.toUTF8(arquivosXinet[(arquivosXinet.length - 1)]);
						String savePath = "";
						if (tipo.equals("layout")) {
							savePath = ConfigBO.PATH + idJob + "_" + idPraca + "/layout";
						} else {
							savePath = ConfigBO.PATH + idJob + "_" + idPraca + "/img";
						}

						if (DownloadFpoBO.getEps(idJob, idPraca, filePath, fileName, savePath)) {
							area.append(fileName + "\n");
						} else {
							area.append("Erro: " + fileName + "\n");
						}

						if (!controle) {
							try {
								fileErrTotal.createNewFile();
							} catch (IOException e) {
								LogHelper.log(e.getMessage());
							}
						}
					}
				} else {
					try {
						fileErrTotal.createNewFile();
					} catch (IOException e) {
						LogHelper.log(e.getMessage());
					}
				}
			} else if (acao.equals("")) {
				LogHelper.log("");
			}
		} else {
			try {
				fileErr.createNewFile();
			} catch (IOException localIOException1) {
			}
			LogHelper.log("Nao foi possivel setar ambiente");
		}

		System.exit(0);
	}
}