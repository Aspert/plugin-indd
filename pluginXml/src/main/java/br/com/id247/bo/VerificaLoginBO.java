package br.com.id247.bo;

import br.com.id247.helpers.HttpConnectionHelper;
import br.com.id247.helpers.LogHelper;
import java.io.File;
import java.io.IOException;

public class VerificaLoginBO {
	public VerificaLoginBO() {
	}

	public static boolean verificaLogin(String usuario, String senha, String coordenadaCartao, String valorCartao) {
		File path = new File(ConfigBO.PATHCONFIG);

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATHCONFIG + ConfigBO.LOGIN);
			if (file.exists()) {
				file.delete();
			}
			File fileErr = new File(ConfigBO.PATHCONFIG + ConfigBO.ERRLOGIN);
			if (fileErr.exists()) {
				fileErr.delete();
			}

			if (!HttpConnectionHelper.getContentOfFile(
					ConfigBO.URLLOGIN + usuario + "/" + senha + "/" + coordenadaCartao + "/" + valorCartao,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATHCONFIG + ConfigBO.PIDLOGIN);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				LogHelper.log(e.getMessage());
			}

			return true;
		}
		LogHelper.log("Nao foi possivel verificar o login");

		return false;
	}
}