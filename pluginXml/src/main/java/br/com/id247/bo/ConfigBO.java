package br.com.id247.bo;

import br.com.id247.helpers.HttpConnectionHelper;
import java.io.File;
import java.io.IOException;

public class ConfigBO {
	public ConfigBO() {
	}

	public static String PATHCONFIG = "";
	public static String BASEURL = "";
	public static String SERVIDOR = "";
	public static String URLXML = "";
	public static String URLXMLINICIAL = "";
	public static String URLXMLPRACAS = "";
	public static String UPDATEJSX = "";
	public static String VALIDATEMPLATEJSX = "";
	public static String URLDOWNLOAD = "";
	public static String URLDOWNLOADLAYOUT = "";
	public static String URLLAYOUTSIZE = "";
	public static String URLLAYOUTDATE = "";
	public static String URLLAYOUTPATH = "";
	public static String URLLOGIN = "";

	public static String URLSCRIPTS = BASEURL + "/IDS/plugin/";

	public static String PATH = "/tmp/";
	public static String ARQUIVO_LOG = PATH + "plugin_retail.log";
	public static String XMLLOCAL = "local.xml";
	public static String XMLINICIAL = "inicial.xml";
	public static String JSXUPDATE = "update_java.jsx";
	public static String PIDUPDATEJSX = "update_java.pid";
	public static String JSXVALIDATEMPLATE = "valida_template.jsx";
	public static String PIDVALIDATEMPLATE = "valida_template.pid";
	public static String XMLPRACAS = "pracas.xml";
	public static String PIDXMLLOCAL = "local.pid";
	public static String PIDXMLINICIAL = "inicial.pid";
	public static String ERRXMLINICIAL = "inicial.err";
	public static String PIDXMLPRACAS = "pracas.pid";
	public static String PIDLAYOUT = "layout.pid";
	public static String LAYOUTSIZE = "size.txt";
	public static String PIDLAYOUTSIZE = "size.pid";
	public static String ERRLAYOUTSIZE = "size.err";
	public static String LAYOUTDATE = "date.txt";
	public static String PIDLAYOUTDATE = "date.pid";
	public static String ERRLAYOUTDATE = "date.err";
	public static String LAYOUTPATH = "path.txt";
	public static String PIDLAYOUTPATH = "path.pid";
	public static String ERRLAYOUTPATH = "path.err";
	public static String VERIFICACONEXAO = "1001";
	public static String LOGIN = "1002";
	public static String PIDLOGIN = "1002.pid";
	public static String ERRLOGIN = "1002.err";

	public static boolean setAmbiente(String servidor, String _path, boolean create) throws IOException {
		if (servidor.equals("sites_h")) {
			BASEURL = "http://retail3h.247id.com.br/";
		} else if (servidor.equals("sites_prod")) {
			BASEURL = "http://retail.247id.com.br/";
		} else {
			BASEURL = servidor;
		}

		BASEURL = VerificaConexaoBO.verificaLinkPrincipal(BASEURL);

		URLXML = BASEURL + "/index.php/importacao_excel/atualiza_xml/";
		URLXMLINICIAL = BASEURL + "/index.php/download_plugin/geraXmlPrimeiroIndd/";
		URLXMLPRACAS = BASEURL + "/index.php/importacao_excel/get_pracas/";
		UPDATEJSX = BASEURL + "/IDS/plugin/update_java.jsx";
		VALIDATEMPLATEJSX = BASEURL + "/IDS/plugin/valida_template.jsx";
		URLDOWNLOAD = BASEURL + "/index.php/download_plugin/getFile1/";
		URLDOWNLOADLAYOUT = BASEURL + "/index.php/download_plugin/getLayout/";
		URLLAYOUTSIZE = BASEURL + "/index.php/download_plugin/getLayoutSize/";
		URLLAYOUTDATE = BASEURL + "/index.php/download_plugin/getLayoutData/";
		URLLAYOUTPATH = BASEURL + "/index.php/download_plugin/getPathTemplate/";
		URLSCRIPTS = BASEURL + "/IDS/plugin/";
		PATH = _path;
		ARQUIVO_LOG = PATH + "/plugin_retail.log";
		URLLOGIN = BASEURL + "/index.php/download_plugin/login/";
		PATHCONFIG = PATH + "config/";

		File path = new File(PATH);
		File pathJsx = new File(PATH + "/jsx/");

		if (((path.isDirectory()) || (path.mkdirs())) && ((pathJsx.isDirectory()) || (pathJsx.mkdirs()))) {
			if (create) {
				File file = new File(PATH + "/jsx/update_java.jsx");
				return HttpConnectionHelper.downloadArquivo(UPDATEJSX, file.getAbsolutePath());
			}
			return true;
		}

		return false;
	}
}