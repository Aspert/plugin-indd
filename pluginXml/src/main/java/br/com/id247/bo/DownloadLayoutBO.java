package br.com.id247.bo;

import br.com.id247.helpers.HttpConnectionHelper;
import br.com.id247.helpers.LogHelper;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;

public class DownloadLayoutBO {
	public DownloadLayoutBO() {
	}

	public static boolean getLayout(String idJob, String idPraca, String idTemplate, String paginaInicio,
			String paginaFim) {
		HttpURLConnection conn = HttpConnectionHelper.getHttpURLConnection(ConfigBO.URLDOWNLOADLAYOUT);
		File path = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout");
		String fileNameSave = path.getAbsolutePath() + "/layout.indd";
		String fileNameErr = path.getAbsolutePath() + "/layout.err";

		if ((path.isDirectory()) || (path.mkdirs())) {
			String params = "&idtemplate=" + idTemplate + "&paginainicio=" + paginaInicio + "&paginafim=" + paginaFim;
			if (!HttpConnectionHelper.getLayout(conn, params, fileNameSave)) {
				File file = new File(fileNameErr);
				try {
					file.createNewFile();
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
				return false;
			}

			File pid = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.PIDLAYOUT);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					return false;
				}
			} catch (IOException e) {
				LogHelper.log(e.getMessage());
			}

			return true;
		}

		return false;
	}

	public static boolean getLayoutSize(String idJob, String idPraca, String idTemplate, String paginaInicio,
			String paginaFim) {
		File path = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/");

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.LAYOUTSIZE);
			if (file.exists()) {
				file.delete();
			}
			File fileErr = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.ERRLAYOUTSIZE);
			if (fileErr.exists()) {
				fileErr.delete();
			}

			if (!HttpConnectionHelper.getContentOfFile(
					ConfigBO.URLLAYOUTSIZE + idTemplate + "/" + paginaInicio + "/" + paginaFim,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.PIDLAYOUTSIZE);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				LogHelper.log(e.getMessage());
			}

			return true;
		}
		LogHelper.log("Nao foi possivel verificar o tamnho do layout");

		return false;
	}

	public static String getLayoutSize1(String idJob, String idPraca, String idTemplate, String paginaInicio,
			String paginaFim) {
		String retorno = "";
		retorno = HttpConnectionHelper
				.getContentOfFile(ConfigBO.URLLAYOUTSIZE + idTemplate + "/" + paginaInicio + "/" + paginaFim);
		return retorno;
	}

	public static boolean getLayoutDate(String idJob, String idPraca, String idTemplate, String paginaInicio,
			String paginaFim) {
		File path = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/");

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.LAYOUTDATE);
			if (file.exists()) {
				file.delete();
			}
			File fileErr = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.ERRLAYOUTDATE);
			if (fileErr.exists()) {
				fileErr.delete();
			}
			if (!HttpConnectionHelper.getContentOfFile(
					ConfigBO.URLLAYOUTDATE + idTemplate + "/" + paginaInicio + "/" + paginaFim,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.PIDLAYOUTDATE);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				LogHelper.log(e.getMessage());
			}

			return true;
		}
		LogHelper.log("Nao foi possivel fazer atualizacao do script");

		return false;
	}

	public static boolean getLayoutPath(String idJob, String idPraca, String idTemplate, String paginaInicio,
			String paginaFim) {
		File path = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/");

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.LAYOUTPATH);
			if (file.exists()) {
				file.delete();
			}
			File fileErr = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.ERRLAYOUTPATH);
			if (fileErr.exists()) {
				fileErr.delete();
			}
			if (!HttpConnectionHelper.getContentOfFile(ConfigBO.URLLAYOUTPATH + idTemplate, file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/layout/" + ConfigBO.PIDLAYOUTPATH);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				LogHelper.log(e.getMessage());
			}

			return true;
		}
		LogHelper.log("Nao foi possivel fazer atualizacao do script");

		return false;
	}
}