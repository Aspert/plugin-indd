package br.com.id247.bo;

import java.io.File;
import java.io.IOException;

public class AtualizaScriptJsxBO {
	public AtualizaScriptJsxBO() {
	}

	public static boolean atualiza(String script) {
		File path = new File(ConfigBO.PATH + "jsx");

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATH + "/jsx/" + script);
			File fileErr = new File(ConfigBO.PATH + "/jsx/" + script + ".err");

			if (file.exists()) {
				file.delete();
			}

			if (!br.com.id247.helpers.HttpConnectionHelper.getContentOfFile(ConfigBO.URLSCRIPTS + script,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					br.com.id247.helpers.LogHelper.log(e.getMessage());
				}
			}

			return true;
		}
		br.com.id247.helpers.LogHelper.log("Nao foi possivel fazer atualizacao do script");

		return false;
	}
}