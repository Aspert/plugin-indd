package br.com.id247.bo;

import java.io.File;
import java.io.IOException;

public class AtualizaXmlPracasLocalBO {
	public AtualizaXmlPracasLocalBO() {
	}

	public static boolean atualiza(String idJob, String idPraca, String idExcel) {
		File path = new File(ConfigBO.PATH + idJob + "_" + idPraca);
		br.com.id247.helpers.DiretorioHelper.deletaRecursivamente(path);
		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATH + "/" + idJob + "_" + idPraca + "/" + ConfigBO.XMLPRACAS);
			File fileErr = new File(ConfigBO.PATH + "/" + idJob + "_" + idPraca + "/" + ConfigBO.XMLPRACAS + ".err");

			if (file.exists()) {
				file.delete();
			}

			if (!br.com.id247.helpers.HttpConnectionHelper.getContentOfFile(ConfigBO.URLXMLPRACAS + idExcel,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					br.com.id247.helpers.LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/" + ConfigBO.PIDXMLPRACAS);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				br.com.id247.helpers.LogHelper.log(e.getMessage());
			}

			return true;
		}
		br.com.id247.helpers.LogHelper.log("Nao foi possivel fazer atualizacao de XML");

		return false;
	}
}