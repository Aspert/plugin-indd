package br.com.id247.bo;

import java.io.File;
import java.io.IOException;

public class AtualizaInicialXmlBO {
	public AtualizaInicialXmlBO() {
	}

	public static boolean atualiza(String idUsuario) {
		File path = new File(ConfigBO.PATHCONFIG);

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATHCONFIG + ConfigBO.XMLINICIAL);
			if (file.exists()) {
				file.delete();
			}
			File fileErr = new File(ConfigBO.PATHCONFIG + ConfigBO.XMLINICIAL);
			if (fileErr.exists()) {
				fileErr.delete();
			}

			if (!br.com.id247.helpers.HttpConnectionHelper.getContentOfFile(ConfigBO.URLXMLINICIAL + "/" + idUsuario,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					br.com.id247.helpers.LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATHCONFIG + ConfigBO.PIDXMLINICIAL);
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				br.com.id247.helpers.LogHelper.log(e.getMessage());
			}

			return true;
		}
		br.com.id247.helpers.LogHelper.log("Nao foi possivel fazer atualizacao de XML");

		return false;
	}
}