package br.com.id247.bo;

import br.com.id247.helpers.HttpConnectionHelper;
import br.com.id247.helpers.LogHelper;
import br.com.id247.helpers.StringHelper;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;

public class DownloadFpoBO {
	public DownloadFpoBO() {
	}

	public static boolean getEps(String idJob, String idPraca, String filePath, String fileName, String savePath) {
		HttpURLConnection conn = HttpConnectionHelper.getHttpURLConnection(ConfigBO.URLDOWNLOAD);
		File path = new File(savePath);
		String fileNameSave = path.getAbsolutePath() + "/" + StringHelper.removeExtensao(fileName) + ".eps";
		String fileNameErr = path.getAbsolutePath() + "/" + StringHelper.removeExtensao(fileName) + ".err";

		if ((path.isDirectory()) || (path.mkdirs())) {
			String params = "&filepath=" + filePath + "&filename=" + fileName;
			if (!HttpConnectionHelper.getEpsFile(conn, params, fileNameSave)) {
				File file = new File(fileNameErr);
				try {
					file.createNewFile();
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
				return false;
			}

			return true;
		}

		return false;
	}
}