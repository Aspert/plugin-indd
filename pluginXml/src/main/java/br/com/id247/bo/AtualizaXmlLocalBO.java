package br.com.id247.bo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JOptionPane;

import br.com.id247.helpers.ArquivoHelper;
import br.com.id247.helpers.DiretorioHelper;

public class AtualizaXmlLocalBO {
	public AtualizaXmlLocalBO() {
	}

	public static boolean atualiza(String tipoAtualizacao, String idJob, String idPraca, String idExcel,
			String paginaInicio, String paginaFim, String argumentos) {
		File path = new File(ConfigBO.PATH + idJob + "_" + idPraca);
		DiretorioHelper.deletaRecursivamente(path);
		
		String tipoAtualizacaoAux = tipoAtualizacao.equals("personalizadoSelecionado")?"selecionado":tipoAtualizacao;
		
		String argumentoAux = tipoAtualizacao.equals("personalizadoSelecionado")?argumentos.substring(0,argumentos.indexOf("||")):argumentos;

		if ((path.isDirectory()) || (path.mkdirs())) {
			File file = new File(ConfigBO.PATH + "/" + idJob + "_" + idPraca + "/" + ConfigBO.XMLLOCAL);
			File fileErr = new File(ConfigBO.PATH + "/" + idJob + "_" + idPraca + "/" + ConfigBO.XMLLOCAL + ".err");

			if (file.exists()) {
				file.delete();
			}
			
			if (!br.com.id247.helpers.HttpConnectionHelper.getContentOfFile(ConfigBO.URLXML + tipoAtualizacaoAux + "/"
					+ idExcel + "/" + idPraca + "/" + paginaInicio + "/" + paginaFim + "/" + argumentoAux,
					file.getAbsolutePath())) {
				try {
					fileErr.createNewFile();
					return false;
				} catch (IOException e) {
					br.com.id247.helpers.LogHelper.log(e.getMessage());
				}
			}

			File pid = new File(ConfigBO.PATH + idJob + "_" + idPraca + "/" + ConfigBO.PIDXMLLOCAL);
			if(tipoAtualizacao.equals("personalizadoSelecionado")){
				ArquivoHelper.removeElementos(file,argumentos.substring(argumentos.indexOf("||") +2));
				
			}
			if (pid.exists()) {
				pid.delete();
			}
			try {
				if (!pid.createNewFile()) {
					fileErr.createNewFile();
					return false;
				}
			} catch (IOException e) {
				br.com.id247.helpers.LogHelper.log(e.getMessage());
			}
			
			return true;
		}
		br.com.id247.helpers.LogHelper.log("Nao foi possivel fazer atualizacao de XML");

		return false;
	}
	
}