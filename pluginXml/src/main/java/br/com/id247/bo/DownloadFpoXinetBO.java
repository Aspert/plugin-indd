package br.com.id247.bo;

import br.com.id247.helpers.HttpConnectionHelper;
import br.com.id247.helpers.LogHelper;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadFpoXinetBO {
	private static HttpURLConnection connection;

	public DownloadFpoXinetBO() {
	}

	public static boolean getFPO() throws MalformedURLException, IOException {
		URL hostUrl = new URL(
				"http://10.247.1.101/webnative/getimage?-fpo+%2Fstorage3%2FBI%2FCLIENTES%2F247_CLIENTE%2FRETAIL%2FBANCO DE IMAGENS%2FPRODUTO%2FVitamassaChocobiskChoco.jpg");
		File path = new File(ConfigBO.PATH);

		connection = (HttpURLConnection) hostUrl.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		String creds = "sidnei.tertuliano:junior321";
		String encodeCreds = Base64.encode(creds.getBytes());
		connection.setRequestProperty("Authorization", "Basic " + encodeCreds);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-length", String.valueOf("valor=21"));
		connection.connect();

		if ((path.isDirectory()) || (path.mkdirs())) {
			String params = "";
			String fileNameSave = path.getAbsolutePath() + "/teste.zip";
			String fileNameErr = path.getAbsolutePath() + "/teste.err";

			if (!HttpConnectionHelper.getEpsFile(connection, params, fileNameSave)) {
				File file = new File(fileNameErr);
				try {
					file.createNewFile();
				} catch (IOException e) {
					LogHelper.log(e.getMessage());
				}
				return false;
			}

			return true;
		}

		return true;
	}
}