package br.com.id247.bo;

import br.com.id247.helpers.HttpConnectionHelper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

public class VerificaConexaoBO {
	public VerificaConexaoBO() {
	}

	public static void verifica() throws IOException {
		File file = new File(ConfigBO.PATHCONFIG + ConfigBO.VERIFICACONEXAO);
		Writer output = new BufferedWriter(new FileWriter(file));

		if (!file.exists()) {
			file.createNewFile();
		}

		if (file.exists()) {
			output.write("0");
			if (HttpConnectionHelper.verificaConexao(ConfigBO.BASEURL)) {
				output.write("1");
			} else {
				output.write("0");
			}
		}
		output.close();
	}

	public static String verificaLinkPrincipal(String url) throws IOException {
		int redirs = 0;
		int maxRedirs = 3;
		HttpURLConnection conn = null;

		while (redirs <= maxRedirs) {
			URL hostUrl = new URL(url);
			conn = (HttpURLConnection) hostUrl.openConnection();
			conn.setRequestMethod("POST");
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.connect();

			int code = conn.getResponseCode();
			if ((code >= 301) && (code <= 307)) {
				url = conn.getHeaderField("location");
			} else {
				if (code == 200) {
					break;
				}

				redirs++;
			}
		}
		return url;
	}
}